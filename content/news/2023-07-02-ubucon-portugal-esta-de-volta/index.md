---
title: "A Ubucon Portugal voltou para nova edição já em 2023"
date: 2023-07-02T17:00:00+01:00
authors:
    - name: Diogo Constantino
      bio: Organizador, Ubucon Portugal 2023 / Membro do Conselho da Comunidade, Ubuntu Portugal
      email: diogoconstantino@ubuntu-pt.org
      launchpad: diogoconstantino
      github: diogoconstantino
      gitlab: diogoconstantino
      profile: https://gitlab.com/uploads/-/system/user/avatar/2796884/avatar.png
      linkurl: https://constantino.io
      linklabel: Website
---

Após uma primeira edição da [Ubucon Portugal em 2019](https://ubuconpt2019.ubuntu-pt.org/), e um interlúdio causado por motivos de força maior, a Ubucon Portugal voltou para uma segunda edição.

Esta nova edição será no **dia 17 de Setembro de 2023** enquadrada no âmbito da [Festa do Software Livre](festa2023.softwarelivre.eu/), que também está a regressar após um interregno de alguns anos.

---

**A Festa do Software Livre 2023** é um evento cuja organização é liderada pela [Associação Nacional para o Software Livre - ANSOL](https://ansol.org), mas que conta com apoio na organização de muitas das mais fantásticas comunidades portuguesas de Software Livre, Software de Código Aberto, Dados Abertos, Direitos Digitais e Cultura Aberta. 
Esta Festa terá lugar **no [Departamento de Electrónica, Telecomunicações e Informática da Universidade de Aveiro](https://www.ua.pt/pt/deti)**, e conta com significativa contribuição na organização por parte do [Grupo de Linux da Universidade de Aveiro](https://glua.ua.pt).

A Ubucon Portugal em si será organizada pelos membros da [Comunidade Ubuntu Portugal](https://loco.ubuntu.com/teams/ubuntu-pt)

---

**O que é a Ubucon Portugal?**

A Ubucon Portugal é a versão portuguesa de um evento do tipo "Ubucon": uma conferência inteiramente organizada pela comunidade Ubuntu, para celebrar o Ubuntu e as suas Comunidades.

**O que acontece numa Ubucon?**

Apresentações sobre Ubuntu, os seus sabores oficiais e derivados; coisas interessantes que fazemos com Ubuntu e com Software Livre; a comunidade Ubuntu em Portugal; como podemos contribuir para tornar o Ubuntu mais útil para mais pessoas; stands de projectos relacionados com Ubuntu e Software Livre - mas principalmente, muito convívio.