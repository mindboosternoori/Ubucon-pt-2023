---
title: "Centro Linux - uma iniciativa da Ubuntu-pt para toda a comunidade"
date: 2023-09-17T10:10:00+01:00
end: 2023-09-17T10:40:00+01:00 #Fim da sessão
room: Sala 4.1.02 (60 pessoas) # Número da sala
#Escolhas Possíveis:
# Palco Principal
# Palco Secundário
# Sala de Workshops
# Zona de Podcasts
category: "#BEB8B2" # Categoria da apresentação
#Escolhas Possíveis:
# "#AEA79F" Warm Grey 100%: Inicio e fim
# "#BEB8B2" Warm Grey 80%: Apresentações
# "#D6D3CF" Warm Grey 50%: Wrokshops
# "#E6E4E2" Warm Grey 30%: Podcasts
# "#F6F6F5" Warm Grey 10%: Outros
lang: Português
featured: true # Se for true, esta sessão irá aparecer na página principal
speakers: # Informação dos oradores
    - name: Diogo Constantino
      bio: Membro do Conselho Comunitário Ubuntu-pt, e Ubuntu Member
      email: diogoconstanino@ubuntu-pt.org  # Email
---

O Centro Linux é uma iniciativa da Ubuntu-pt para toda a comunidade. Vem saber o que lá acontece; como pretendemos que fortaleça a comunidade de Software Livre em Portugal e como permitir a organização de outras comunidades (até mesmo de outras distribuições de GNU/Linux!).
